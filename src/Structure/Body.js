import leaf from '../criLeaf.png';
import { replaceCellValue } from '../Utilities'
import { AppContext } from "../App";
import { useContext } from "react";

export const Body = () => {
	return(
		<div className="body-container">
			<div className="body-content p-3 pt-10 pb-32">
				<TextContainer      />
				<GeneratorControls  />
				<RowGrid            />
				<ArrayResult        />
			</div>
		</div>
	)
}

const TextContainer = () => {
	const {controls, setControls} = useContext(AppContext)
	const toggleDarkEnabled = () => {setControls({...controls,darkEnabled:!controls.darkEnabled})}
	return(<div>
			<button type="button" onClick={() => toggleDarkEnabled()}
			        className="btn text-xs bg-gray-700 text-gray-50 float-right">
				Toggle<br/>Dark Mode
			</button>
			<div className="leaf-container my-5 w-20 mx-auto">
				<img src={leaf} className="leaf-logo" alt="CRI leaf" />
			</div>
			<p className="text-justify my-5">Manually entering the class times and titles for
				the <a href="http://recreation.georgiasouthern.edu/fitness/fitness-programs/group-fitness-schedule/">
				CRI Group Fitness schedule</a> can feel tedious and annoying. This app to make it easier on you! Use this to
				generate the necessary arrays for the <em>fall</em>, <em>spring</em>, <em>summerA</em>,&nbsp;
				<em>summerB</em>, and <em>finals-week</em> schedules.
			</p>
		</div>)
}

const GeneratorControls = () => {
	const {controls, setControls} = useContext(AppContext)
	const fieldClass = "w-full border h-8 p-0.5"
	const modifyControls = (prop) => { setControls( { ...controls, ...prop } ) }
	return <>

		<hr className="my-2"/>
		<div className="flex text-center">
				<div className="w-1/5">
					<p>Declaration</p>
					<select className={fieldClass} defaultValue={controls.declaration}
					        onChange={(it) =>
						        modifyControls({declaration:it.target.value})}>
						<option  value={"var"}>var</option>
						<option  value={"let"}>let</option>
						<option  value={"const"}>const</option>
					</select>
				</div>
				<div className="w-1/5">
					<p>Row prefix</p>
					<input className={fieldClass} defaultValue={controls.rowPrefix}
					       onChange={(it) =>
						       modifyControls({rowPrefix:it.target.value})}/>
				</div>
				<div className="w-1/5">
					<p>Delimiter</p>
					<select className={fieldClass} defaultValue={controls.delimiter}
					        onChange={(it) =>
						        modifyControls({delimiter:it.target.value})}>
						<option  value={"pipe"}>Pipe (|)</option>
						<option  value={"comma"}>Comma (,)</option>
					</select>
				</div>
				<div className="w-1/5">
					<p>Quotes</p>
					<select className={fieldClass} defaultValue={controls.quotes}
					        onChange={(it) =>
						        modifyControls({quotes:it.target.value})}>
						<option  value={"single"}>Single</option>
						<option  value={"double"}>Double</option>
						<option  value={"none"}>None</option>
					</select>
				</div>
				<div className="w-1/5">
					<p>Brackets</p>
					<select className={fieldClass} defaultValue={controls.brackets}
					        onChange={(it) =>
						        modifyControls({brackets:it.target.value})}>
						<option  value={"curly"}>Curly</option>
						<option  value={"square"}>Square</option>
					</select>
				</div>
			</div>
		<hr className="my-4"/>
	</>
}

const RowGrid = () => {
	const {rows, setRows} = useContext(AppContext)
	const gridContent = rows.map((row, rowIdx) => {
		const rowPrefix = `${rowIdx}`
		return <div className="flex justify-around text-xs" id={`row${rowPrefix}`} key={rowPrefix}>
			{row.map((colData, colIdx) => {
				return <input type="text"   defaultValue={colData}  id={`${rowPrefix}_${colIdx}`}
				              className="border w-20 m-0.5 p-0.5"   key={colIdx}  name={`${rowPrefix}-${colIdx}`}
				              onChange={(it) => {
												replaceCellValue(it.target, rows, setRows)
				              }}/>
			})}
		</div>
	})
	return <div className="overflow-x-scroll">
		{gridContent}
	</div>

}

const ArrayResult = () => {
	const {arrayResult, textAreaRef} = useContext(AppContext)
	return <div>
		<hr className="my-4"/>
		<textarea id="array-result" className="w-full border-2 text-xs font-mono" rows="10" defaultValue={arrayResult} ref={textAreaRef}/>
		<p className="mt-4 italic text-center text-gray-700 dark:text-gray-50">Get Fit. Get Group Fit!</p>
	</div>
}



