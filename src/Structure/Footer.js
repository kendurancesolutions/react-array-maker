import { removeLastRow, addRow, generateArray, clearGrid } from '../Utilities'
import { AppContext } from "../App";
import { useContext } from "react";

export const Footer = () => {
	const { controls, rows, setRows, setArrayResult, textAreaRef } = useContext(AppContext)
	return(
		<footer className="footer-container">
			<div className="button-container text-xs">
				<div>
					<button type="button" onClick={() => removeLastRow(rows, setRows)} className="mr-1">-</button>
					<button type="button" onClick={() => addRow(rows, setRows, controls.headerRow)}>+</button>
				</div>
				<div>
					<button type="button" onClick={() => generateArray(controls, rows, setArrayResult, textAreaRef)}>Create Schedule</button>
				</div>
				<div>
					<button type="button" onClick={() => clearGrid(setArrayResult)}>Clear Result</button>
				</div>
			</div>
		</footer>
	)
}