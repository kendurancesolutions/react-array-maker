/**
 *  App/Top-level functions
 */

export const rowGenerator  = (headerRow, numberOfRows) => {
	const fillerRow         = new Array(headerRow.length).fill("")
	const generatedArray    = new Array(numberOfRows).fill(fillerRow)
	generatedArray.unshift(headerRow) // prepending filler row to generated array
	return generatedArray
}

export const replaceCellValue = (ele, rows, setRows) => {
	const val = ele.value
	const [rowPos, colPos] = ele.id.toString().split('_').map((it) => {
		return parseInt(it)
	}) // [row#][col#]
	if(rowPos && colPos){
		setRows(rows.map((row, rowIdx) => {
			return rowIdx !== rowPos ? row : row.map((cell, cellIdx) => {
				return cellIdx === colPos ? val : cell
			})
		}))
	}
}

/**
 *  Array/grid functions
 */

export const addRow = (rows, setRows, headerRow) => {
	setRows([...rows,new Array(headerRow.length).fill("")])
}

export const removeLastRow = (rows, setRows) => {
	if(rows.length > 2){
		rows.pop()
		setRows([...rows])
	}
}

export const generateArray = (controls, rows, setArrayResult, textAreaRef) => {
	const { declaration, rowPrefix, brackets, delimiter, quotes } = controls
	const addQuotes     = (cell) => {
		switch(quotes){
			case 'single':  return `'${cell}'`
			case 'double':  return `"${cell}"`
			default:        return cell
		}
	}
	const addBrackets   = (placement) => {
		return{
			curly:  {beginning:'{', ending:'}'},
			square: {beginning:'[', ending:']'},
		}[brackets][placement]
	}
	const addDelimiter  = () => {
		return {
			comma:  ', ',
			pipe:   ' | ',
		}[delimiter]
	}

	setArrayResult(rows.map((row, rowIdx) => {
		return(
			`${declaration} ${rowPrefix+rowIdx} \t= ${addBrackets('beginning')}${row?.map((cell) => {
				return addQuotes(cell)
			})?.join(addDelimiter())}${addBrackets('ending')};\n`
		)
	}).join(""))

	changeRows_ArrayResult(rows, textAreaRef)
}

export const clearGrid = (setArrayResult) => {
	setArrayResult("")
}

export const changeRows_ArrayResult = (rows, textAreaRef) => {
	const arrayResult = textAreaRef.current
	if(rows.length) arrayResult.rows = rows.length + 2
}