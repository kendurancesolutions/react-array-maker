import { Header, Body, Footer } from "./Structure"
import {createContext, useCallback, useRef, useState} from "react";
import { rowGenerator } from './Utilities';

export const AppContext = createContext(null)

const App = () => {
  const [ arrayResult, setArrayResult]  = useState("")
  const [ controls, setControls ]       = useState({
    declaration:"let", darkEnabled: false, rowPrefix:"row",  delimiter:"comma",  quotes:"double", brackets:"square",
    headerRow: ["Time/Day","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],
  })
  const memo_rowGenerator = useCallback( () => rowGenerator(controls.headerRow, 10), [controls.headerRow])
  const [ rows, setRows ]               = useState(memo_rowGenerator)
  const darkClass   = controls.darkEnabled ? 'dark' : ''
  const textAreaRef = useRef()
  return (
    <div className={`App ${darkClass}`}>
      <AppContext.Provider value={{ controls, setControls, rows, setRows, arrayResult, setArrayResult, textAreaRef }}>
        <Header />
        <Body   />
        <Footer />
      </AppContext.Provider>
    </div>
  );
}


export default App