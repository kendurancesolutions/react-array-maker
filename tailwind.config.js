
module.exports = {
  //mode: 'jit',
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'class', // or 'media' or false
  theme: {
    extend: {
      colors: {
        'gsu': {
          DEFAULT: '#18315A',
          'dark': '#122543',
          'light': '#2d65da',
          'text': '#f3f3f3',
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
